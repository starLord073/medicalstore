import React from "react"

class SearchItems extends React.Component{
    constructor(props){
        super(props)
        this.state={
            searchName : "a",
            returnData : null,
        }
        this.handleButtonClick=this.handleButtonClick.bind(this)
        this.handleChange=this.handleChange.bind(this)
    }

    

    handleChange(event){
        // console.log(event.target.value)
        var val = event.target.value
        this.setState((prevState) => {
            return{
                searchName : val,
                returnData : prevState.returnData
            }
        })
        // console.log(this.state)
    }

    handleButtonClick(event){
        // console.log("In handle click")
        // console.log('http://www.omdbapi.com/?i=tt389619&apikey=ca6bbc53&s='+this.state.searchName)


        fetch('http://www.omdbapi.com/?i=tt389619&apikey=ca6bbc53&s='+this.state.searchName)
        .then(response => response.json())
        .then( (data) => {
            // console.log(data.Search)
            this.setState((prevState)=>{
                // console.log("Going to set state")
                return {
                    searchName : prevState.searchName,
                    returnData : data.Search
                }
            })
            console.log(this.state)
        });
    }



    render(){
        return(
            <div>
                <br/>
                <br/>
                    <input 
                        className="form-control" 
                        type="text" 
                        placeholder="Search" 
                        aria-label="Search"
                        onChange={this.handleChange}
                        />
                    <br/><br/>
                    
                    <button 
                    className="btn btn-light"
                    style={{marginLeft:'45%'}}
                    onClick={this.handleButtonClick}>SEARCH</button>
                    {console.log(this.state)}
            </div>
        )
    }
}


export default SearchItems